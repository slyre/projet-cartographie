Projet Cartographie

L’objectif est de programmer une application de cartographie permettant d’exploiter des données ouvertes « open data » proposées par la ville de Chambéry.

https://donnees.grandchambery.fr/

En bonus et afin d’alimenter l’application avec des informations complémentaires, il sera également envisageable d’exploiter les données collectées par l’intermédiaire du projet « bot ».

*Durée :* 2 semaines

*Groupe *: projet individuel

*Objectifs *:

-   Maquetter une application et réalisation d’un scénario utilisateur
-   Approfondissement des mécanismes asynchrones et plus particulièrement de type Ajax via l’utilisation de l’API Javascript « Fetch »
-   Découverte de « open data » et de l’utilisation de services web exploitant un type d’architecture REST
-   Prise en main de l’outil WebPack pour améliorer l’organisation du code source Javascript par l’intermédiaire de l’instruction « import »
-   Découverte de « OpenStreetMap » et de son utilisation par l’intermédiaire de la librairie de carte interactive Leaflet

Compétences :

-   Front-end : Maquetter une application
-   Front-end : Réaliser une interface utilisateur web statique et
-   adaptable
-   Front-end : Développer une interface utilisateur web dynamique

Contraintes :

-   L’ensemble du code source (nom des variables, fonctions) et des commentaires doivent être en anglais.
-   Les données « open data » doivent obligatoirement être exploitées via les services web REST par l’intermédiaire de fetch.

*Projet *:

Analyse et sélection des données « open data » proposées par la ville de Chambéry.

Conception d’une maquette et d’un scénario utilisateur permettant d’exploiter les données précédemment sélectionnées. Ne pas oublier de prendre en considération l’affichage des informations pour différentes gammes d'appareils (mobiles, tablettes et ordinateurs de bureau).

L’objectif principal est de récupérer les données par l’intermédiaire des services web proposées sur le site : [https://donnees.grandchambery.fr](https://donnees.grandchambery.fr/)

Ces données devront être affichées sur une carte interactive par l’intermédiaire de la librairie Javascript Leaflet. Afin de simplifier l’organisation de votre code javascript, il est fortement recommandé d’exploiter l’outil webpack et l’instruction « import » introduit avec l’arrivée de ECMAScript 6.

Bonus :

-   Alimenter l’application avec des informations complémentaires en exploitant les données collectées par l’intermédiaire du projet « bot »
-   Transpiler son code ES6/7 en ES5 pour s’assurer de son exécution sur les anciens navigateurs
-   Optimiser sa configuration WebPack pour une prise en charge de la minification, watch, gestion d’une configuration pour un environnement de dev ou de prod
