//creation of an icon of a new color
const greenIcon = L.icon({
  iconUrl: 'img/marker-icon-green.png',
  iconRetinaUrl: 'img/marker-icon-2x-green.png',
  shadowUrl: 'img/marker-shadow.png',

});
const violetIcon = L.icon({
  iconUrl: 'img/marker-icon-violet.png',
  iconRetinaUrl: 'img/marker-icon-2x-violet.png',
  shadowUrl: 'img/marker-shadow.png',


});
const redIcon = L.icon({
  iconUrl: 'img/marker-icon-red.png',
  iconRetinaUrl: 'img/marker-icon-2x-red.png',
  shadowUrl: 'img/marker-shadow.png',
});
const orangeIcon = L.icon({
  iconUrl: 'img/marker-icon-orange.png',
  iconRetinaUrl: 'img/marker-icon-2x-orange.png',
  shadowUrl: 'img/marker-shadow.png',
});


// featureGroup is useful to apply the same behavior to different groups of layers.
let legend = L.control({
  position: "topright"
});
let layerGroups = L.featureGroup();
let layerMarker = L.layerGroup();
let radius;
let userCoordinates;
let dataRestaurant;
let dataParking;
let dataBus;
let dataVelo;



// Chercher ne doit servir qu'à chercher les trucs autour
// Explication sur comment se servir du site



// Initialize the map.
function initMap() {
  // Create the object 'myMap' and insert in HTML with the mapid ID.
  myMap = L.map('mapid').setView([45.598173, 5.919573], 11);

  // We get the tiles with openstreetmap so we tell to 'Leaflet' where we get informations. 
  L.tileLayer('https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {

    attribution: 'données © <a href="//osm.org/copyright">OpenStreetMap</a>/ODbL - rendu <a href="//openstreetmap.fr">OSM France</a>',
    minZoom: 1,
    maxZoom: 20
  }).addTo(myMap);

}

// TODO
// regrouper le bouton géolocaliser dans une partie 'géolocalisation' + mettre la consigne avec clique gauche = repère


// loading data
function loadData(url) {
  //   try {
  //     let response = await fetch(url);
  //     if (response.ok) {
  //       let json = await response.json();
  //       getData(json, url);
  //     } else {
  //       console.log(`Network request failed with response ${response.status}: ${response.statusText}`);
  //     }

  //   } catch (err) {
  //     console.log('Fetch Error :-S', err);
  //   }
  // }
  // Promise.all([promise1, promise2, promise3]).then(function(values) {
  //   console.log(values);
  // });Promise.all([promise1, promise2, promise3]).then(function(values) {
  //   console.log(values);
  // });
  return fetch(url)
    .then(response => {
      if (response.ok) {
        return response.json()
          .then(data => {

            getData(data, url);
          });
      } else {
        console.log(`Network request failed with response ${response.status}: ${response.statusText}`);
      }
    })
    .catch(err => {
      console.log('Fetch Error :-S', err);
    })
}
// display only fastfood informations
function getData(myData, url) {
  switch (url) {
    case "https://donnees.grandchambery.fr/api/records/1.0/search/?dataset=base-sirene&rows=291&facet=rpet&facet=depet&facet=libcom&facet=siege&facet=libapet&facet=libtefet&facet=saisonat&facet=libnj&facet=libapen&facet=ess&facet=libtefen&facet=categorie&facet=proden&facet=libtu&facet=liborigine&facet=libtca&facet=libreg_new&facet=nom_dept&facet=section&refine.libcom=CHAMBERY&refine.libapet=Restauration+traditionnelle&refine.libapet=Restauration+de+type+rapide":
      dataRestaurant = myData;
      break;
    case "https://donnees.grandchambery.fr/api/records/1.0/search/?dataset=parkings-enclos-ou-ouvrage-a-chambery&rows=17&facet=delegataire&facet=secteur&facet=type_park&facet=remarque":
      dataParking = myData;
      break;
    case "https://donnees.grandchambery.fr/api/records/1.0/search/?dataset=t_troncon_shape&rows=467&facet=code_ligne&facet=commune&facet=nom":
      dataBus = myData;
      break;
    case "../src/velo.geojson":
      dataVelo = myData;
      break;
  }
}

function displayRestaurants(dataRestaurant) {
  checkBoxFastFood = document.getElementById("fastfood");
  checkBoxTraditional = document.getElementById("traditional");
  let myProperties = dataRestaurant.records;

  for (let i = 0; i < myProperties.length; i++) {

    if (myProperties[i].fields.coordonnees && myProperties[i].fields.l2_normalisee) {

      let coordinates = L.latLng(myProperties[i].fields.coordonnees);
      let name = "Restaurant :\n" + myProperties[i].fields.l2_normalisee;
      let address = "Adresse :\n" + myProperties[i].fields.l4_normalisee;
      let distance = coordinates.distanceTo(userCoordinates);

      if (distance <= radius) {
        if (myProperties[i].fields.libapet === "Restauration de type rapide" && checkBoxFastFood.checked == true) {
          createMarkers(coordinates, name, address, orangeIcon);
        } else if (myProperties[i].fields.libapet === "Restauration traditionnelle" && checkBoxTraditional.checked == true) {
          createMarkers(coordinates, name, address, violetIcon);
        }
      }
    }
  }
  layerGroups.addTo(myMap);
}
// display markers for parking only
function displayParking(dataParking) {
  checkBoxParking = document.getElementById("parking");
  let myProperties = dataParking.records;

  for (let i = 0; i < myProperties.length; i++) {

    if (myProperties[i].fields.coordonnees_gps && myProperties[i].fields.nom_park && checkBoxParking.checked == true) {

      let coordinates = L.latLng(myProperties[i].fields.coordonnees_gps);
      let name = "Nom :\n" + myProperties[i].fields.nom_park;
      let address = "Adresse :\n" + myProperties[i].fields.adresse;
      let distance = coordinates.distanceTo(userCoordinates);
      if (distance <= radius) {
        createMarkers(coordinates, name, address, greenIcon);

      }
    }
  }
  layerGroups.addTo(myMap);
}

function displayVelo(dataVelo) {
  checkBoxVelo = document.getElementById("velo");

  if (checkBoxVelo.checked == true) {
    L.geoJSON(dataVelo).addTo(layerGroups);
    layerGroups.addTo(myMap);
  }
}

function displayBus(dataBus) {
  checkBoxBus = document.getElementById("bus");
  let myProperties = dataBus.records;

  for (let i = 0; i < myProperties.length; i++) {

    if (myProperties[i].fields.geo_point_2d && myProperties[i].fields.nom && checkBoxBus.checked == true) {

      let coordinates = L.latLng(myProperties[i].fields.geo_point_2d);
      let name = "Arrêt :\n" + myProperties[i].fields.nom;
      let distance = coordinates.distanceTo(userCoordinates);
      let address = "Ligne :\n" + myProperties[i].fields.code_ligne;
      if (distance <= radius) {
        createMarkers(coordinates, name, address, redIcon);

      }
    }
  }
  layerGroups.addTo(myMap);
}

// put markers only for Fast Food (orange markers)
function createMarkers(coordinates, name, address, color) {
  // generic function to display markers on the map
  marker = new L.marker(coordinates, {
    icon: color
  }).addTo(layerGroups);
  marker.bindPopup(`<b>${name}</b><br>${address}`).openPopup();
}


function modifyRadius(meters) {
  radius = meters;
}

function clearMarkers(layer) {
  // clear all markers thanks to the featureGroup
  layer.clearLayers(myMap);
}

// Get L.LatLng object of the circle

function geolocalization(e) {

  document.getElementById("1km").disabled = false;
  document.getElementById("3km").disabled = false;
  document.getElementById("5km").disabled = false;
  document.getElementById("bus").disabled = false;
  document.getElementById("parking").disabled = false;
  document.getElementById("velo").disabled = false;
  document.getElementById("fastfood").disabled = false;
  document.getElementById("traditional").disabled = false;
  document.getElementById("search").disabled = false;

  userCoordinates = L.latLng(e.latitude, e.longitude);
  L.marker(e.latlng).addTo(layerGroups);

  L.circle(e.latlng, radius).addTo(layerGroups);
  layerGroups.addTo(myMap);
}

function createManuallyMarker(markerCoordinates) {
  // Add marker to map at click location; add popup window

  userCoordinates = L.latLng(markerCoordinates);
  marker = new L.marker(userCoordinates).addTo(layerMarker);
  L.marker(userCoordinates).addTo(layerMarker);
  L.circle(userCoordinates, radius).addTo(layerMarker);
  layerMarker.addTo(myMap);
}

function onLocationError(e) {
  document.getElementById('errorGeo').innerHTML = "Veuillez positionner manuellement un marqueur sur la carte à l'aide du clique gauche de la souris.";
  document.querySelector('#errorGeo').style.display = 'block';
}

// Part of events

function showMap() {
  $('#map-id').show();
}

function verifyIfMarkerOrBoxChecked() {
  checkBoxBus = document.getElementById("bus");
  checkBoxParking = document.getElementById("parking");
  checkBoxVelo = checkBoxVelo = document.getElementById("velo");
  checkBoxFastFood = document.getElementById("fastfood");
  checkBoxTraditional = document.getElementById("traditional");

  if (checkBoxBus.checked == false &&
    checkBoxVelo.checked == false &&
    checkBoxParking.checked == false &&
    checkBoxTraditional.checked == false &&
    checkBoxFastFood.checked == false) {
    document.getElementById('error').innerHTML = "Veuillez cocher au moins une option de recherche !";
    document.querySelector('#error').style.display = 'block';
    
  } else {
    document.querySelector('#error').style.display = 'none';
    
  }
}

window.addEventListener("load", async function () {
  // Functions executed only when the DOM is loaded. 

  $.fancybox.open('<div class="instructions"><h2>Avant de commencer :</h2><p>Veillez à bien vous géolocaliser à l\'aide du bouton "Me Géolocaliser" ou en plaçant un repère en cliquant sur la carte !</p></div>');
  await loadData('https://donnees.grandchambery.fr/api/records/1.0/search/?dataset=base-sirene&rows=291&facet=rpet&facet=depet&facet=libcom&facet=siege&facet=libapet&facet=libtefet&facet=saisonat&facet=libnj&facet=libapen&facet=ess&facet=libtefen&facet=categorie&facet=proden&facet=libtu&facet=liborigine&facet=libtca&facet=libreg_new&facet=nom_dept&facet=section&refine.libcom=CHAMBERY&refine.libapet=Restauration+traditionnelle&refine.libapet=Restauration+de+type+rapide');
  await loadData('https://donnees.grandchambery.fr/api/records/1.0/search/?dataset=parkings-enclos-ou-ouvrage-a-chambery&rows=17&facet=delegataire&facet=secteur&facet=type_park&facet=remarque');
  await loadData('../src/velo.geojson');
  await loadData('https://donnees.grandchambery.fr/api/records/1.0/search/?dataset=t_troncon_shape&rows=467&facet=code_ligne&facet=commune&facet=nom');
  showMap();
  initMap();
  myMap.on('locationerror', onLocationError);
  legend.onAdd = function () {
    let div = L.DomUtil.create("div", "legend");
    div.innerHTML += "<h4>Légende</h4>";
    div.innerHTML += '<i style="background: #377eb8"></i><span>Moi</span><br>';
    div.innerHTML += '<i style="background: #de2d26"></i><span>Arrêts de bus</span><br>';
    div.innerHTML += '<i style="background: #4daf4a"></i><span>Parkings</span><br>';
    div.innerHTML += '<i style="background: #984ea3"></i><span>Restaurant Traditionnel</span><br>';
    div.innerHTML += '<i style="background: #ff7f00"></i><span>Restaurant Fast Food</span><br>';
    div.innerHTML += '<i class="icon" style="background-image: url(http://worldartsme.com/images/blue-line-clipart-1.jpg);background-repeat: no-repeat;"></i><span>Vélo</span><br>';
    return div;
  };
  legend.addTo(myMap);


  modifyRadius(1000);





  document.getElementById("1km").onclick = function () {
    modifyRadius(1000);
  };
  document.getElementById("3km").onclick = function () {
    modifyRadius(3000);
  };
  document.getElementById("5km").onclick = function () {
    modifyRadius(5000);
  };

  // functions onclick
  document.getElementById("geolocalization").onclick = function () {
    // calling the clear on each click to be sure that markers don't stack
    document.querySelector('#errorGeo').style.display = 'none';
    myMap.locate({
      setView: true,
      maxZoom: 14
    });
    clearMarkers(layerGroups);
    clearMarkers(layerMarker);
    myMap.on('locationfound', geolocalization);
  };




  myMap.on('click', function (e) {
    document.querySelector('#errorGeo').style.display = 'none';
    document.getElementById("1km").disabled = false;
    document.getElementById("3km").disabled = false;
    document.getElementById("5km").disabled = false;
    document.getElementById("bus").disabled = false;
    document.getElementById("parking").disabled = false;
    document.getElementById("velo").disabled = false;
    document.getElementById("fastfood").disabled = false;
    document.getElementById("traditional").disabled = false;
    document.getElementById("search").disabled = false;
    myMap.setView(e.latlng, 14);
    clearMarkers(layerGroups);
    clearMarkers(layerMarker);
    createManuallyMarker(e.latlng);
  });

  document.getElementById("search").onclick = function () {
    clearMarkers(layerMarker);
    clearMarkers(layerGroups);
    verifyIfMarkerOrBoxChecked();
    createManuallyMarker(userCoordinates);
    displayRestaurants(dataRestaurant);
    displayParking(dataParking);
    displayVelo(dataVelo);
    displayBus(dataBus);
  };



});