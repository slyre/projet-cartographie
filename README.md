# Projet Cartographie

2 branches : master / dev

## 1 

Décider du contenu : 

Comment se déplacer sur Chambéry en fonction de son moyen de déplacement.
Vélo / Voiture / Bus

(pour accéder aux restaurants si j'arrive à aller jusqu'aux bonus)

* Arrêts de bus du réseau de transport en commun Stac 
* Aménagements cyclables Grand Chambéry
* Parkings enclos ou ouvrage à Chambéry

* Arrêts et horaires théoriques bus (optionnel si tout se passe bien)


## 2 

Gestion du projet : 

* intégration maquette 2 DJ
* gestion de la carte (leafflet / OSM) - 7 DJ
 - mettre une carte
 - intéragir avec les fonctionnalités
 - fixer des points GPS
 
* récup données et les croiser / JSON / BDD 1 DJ
* test/correction 2 DJ


## 3 

Scénario utilisateur : 

*  Sur papier

## 4

Wireframe :

* Desktop : https://wireframe.cc/meUy93

* Mobile : https://wireframe.cc/2DcG2a

* Tablet : https://wireframe.cc/fmraJI
